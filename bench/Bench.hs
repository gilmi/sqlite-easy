{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StandaloneDeriving #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

import Criterion.Main as Criterion

import GHC.Generics (Generic)
import Control.DeepSeq (NFData)
import Control.Monad
import Database.Sqlite.Easy


deriving instance NFData  SQLData

data BenchEnv = BenchEnv
  { largeSampleRows :: [[SQLData]]
  , smallSmapleRows :: [[SQLData]]
  } deriving (Generic, NFData)

main :: IO ()
main = defaultMain
  [ env setupEnv $ \ ~(BenchEnv{..}) ->
    bgroup "sqlite-easy-insert" 
        [ bench "1000 rows/individual"     $ insertBench False smallSmapleRows
        , bench "1000 rows/bulk"           $ insertBench True  smallSmapleRows
        , bench "100000 rows/individual"   $ insertBench False largeSampleRows
        , bench "100000 rows/bulk"         $ insertBench True  largeSampleRows
        ]
  ]

setupEnv :: IO BenchEnv
setupEnv =
  pure $ BenchEnv (take 100000 rows) (take 1000 rows)
  where
    rows = cycle
      [ [SQLInteger 42, SQLFloat 3.14152,   SQLText "The quick brown fox jumps over the lazy dog."]
      , [SQLInteger  0, SQLFloat 12.131516, SQLText "If you've got nothing to say, start a band."]
      ]

createTable, insertTable :: SQL
createTable = "CREATE TABLE bench (b_int INTEGER, b_float REAL, b_text TEXT)"
insertTable = "INSERT INTO bench VALUES (?,?,?)"

insertBench :: Bool -> [[SQLData]] -> Benchmarkable
insertBench bulkMode rows = nfIO $
  withDb ":memory:" $ do
    run createTable
    void $ transaction $ if bulkMode
      then insertTable `runWithMany` rows
      else mapM (insertTable `runWith`) rows
