sqlite-easy
===========

This library provides a primitive yet easy to use api for SQLite3 using the `direct-sqlite` library,
resource pooling using `resource-pool`, and migrations using the `migrant` library.

It might be useful for small toy projects and save a tiny bit of boilerplate.

- [See the docs for more information](https://hackage.haskell.org/package/sqlite-easy)
- [See an example app](https://github.com/soupi/sqlite-easy-example-todo)
